variable "repository_name" {
  description = "Common name, a unique identifier"
  type        = string
}

variable "kms_key_arn" {
  description = "AWS KMS ARN"
  type        = string
  default     = null
}

variable "extra_tags" {
  description = "Map of additional tags to add to module's resources"
  type        = map(string)
  default     = {}
}
