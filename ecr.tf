resource "aws_ecr_repository" "repository" {
  name = var.repository_name

  dynamic "encryption_configuration" {
    for_each = {
      encryption = { type = var.kms_key_arn != null ? "KMS" : "AES256", kms_key_arn = var.kms_key_arn }
    }
    content {
      encryption_type = encryption_configuration.value["type"]
      kms_key         = encryption_configuration.value["kms_key_arn"]
    }
  }

  image_scanning_configuration {
    scan_on_push = true
  }

  tags = merge(local.common_tags, var.extra_tags)
}

resource "aws_ecr_lifecycle_policy" "default" {
  repository = aws_ecr_repository.repository.name
  policy     = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Delete all untagged images",
            "selection": {
                "tagStatus": "untagged",
                "countType": "sinceImagePushed",
                "countUnit": "days",
                "countNumber": 3
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}
