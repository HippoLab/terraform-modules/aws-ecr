Terraform AWS ECR Module
========================

Terraform module to create following AWS resources:
- ECR
- Default ECR policy

# Contents
- [Required Input Variables](#variables)
- [Usage](#usage)
- [Outputs](#outputs)
- [Licence](#licence)
- [Author Information](#author)

## <a name="variables"></a> Required Input Variables
At least following input variables must be provided. See [full list](variables.tf) of supported variables

| Name | Description                      |
| ---- | -------------------------------- |
| name | Common name, a unique identifier |

## <a name="usage"></a> Usage

To provision default configuration use:
```hcl-terraform
module "frontend_repository" {
  source          = "./modules/aws_ecr"
  repository_name = "frontend"
}
```

For more complex configurations with KMS encryption enabled and/or additional tags applied use:

```hcl-terraform
module "frontend_repository" {
  source          = "./modules/aws_ecr"
  repository_name = "frontend"
  kms_key_arn     = aws_kms_key.ecr.arn
  extra_tags      = var.tags
}

resource "aws_kms_key" "ecr" {
  description = "ECR KMS Key"
  tags        = var.tags
}

resource "aws_kms_alias" "ecr" {
  name          = "alias/ecr"
  target_key_id = aws_kms_key.ecr.key_id
}
```

## <a name="outputs"></a> Outputs
Full list of module's outputs and descriptions can be found in [outputs.tf](outputs.tf)

## <a name="license"></a> License
The module is being distributed under [MIT Licence](LICENCE.txt). Please make sure you have read, understood and agreed
to its terms and conditions

## <a name="author"></a> Author Information
Vladimir Tiukhtin <vladimir.tiukhtin@hippolab.ru><br/>London
